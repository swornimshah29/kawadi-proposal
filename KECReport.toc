\contentsline {chapter}{Approval Letter}{i}{chapter*.2}
\contentsline {chapter}{Abstract}{ii}{chapter*.3}
\contentsline {chapter}{Acknowledgment}{iii}{chapter*.4}
\contentsline {chapter}{List of Abbreviations}{vii}{chapter*.7}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Problem Statement}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Objective}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Application}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Features}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Feasibility Analysis}{4}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Economic feasibility }{4}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}Technical feasibility }{4}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}Operational feasibility }{5}{subsection.1.6.3}
\contentsline {section}{\numberline {1.7}System Requirement}{5}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}Software Requirement }{5}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}Hardware Requirement }{5}{subsection.1.7.2}
\contentsline {chapter}{\numberline {2}Literature Review}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Background}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}Methodology}{10}{chapter.3}
\contentsline {section}{\numberline {3.1}Prototype model}{10}{section.3.1}
\contentsline {section}{\numberline {3.2}Chain Algorithm}{11}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}User perspective}{12}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Pickers perspective}{12}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Use case diagram}{14}{section.3.3}
\contentsline {chapter}{\numberline {4}Results and Discussion}{16}{chapter.4}
\contentsline {section}{\numberline {4.1}Output}{16}{section.4.1}
\contentsline {section}{\numberline {4.2}Limitations}{17}{section.4.2}
\contentsline {section}{\numberline {4.3}Problems Faced}{18}{section.4.3}
\contentsline {section}{\numberline {4.4}Work Schedule}{18}{section.4.4}
\contentsline {chapter}{References}{18}{figure.caption.16}
